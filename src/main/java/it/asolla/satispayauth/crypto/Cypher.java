/**
 * 
 */
package it.asolla.satispayauth.crypto;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.asolla.satispayauth.exceptions.EncryptionException;

/**
 * This class provides a method to encrypt a string with the SHA256withRSA algorithm
 * 
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class Cypher {
	
	private static final Logger LOGGER = LogManager.getLogger(Cypher.class);

	/**
	 * Encrypt the input string with the SHA256withRSA algorithm. 
	 * The encrypted string is then converted to a base64 string
	 *
	 * @param input the input string
	 * @return the base64 representation of the encrypted string
	 */
	public String encrypt(String input) {
		LOGGER.info("Encrypting...");
        try{
        	 Signature privateSignature = Signature.getInstance("SHA256withRSA");
             privateSignature.initSign(getPrivateKey());
             privateSignature.update(input.getBytes(StandardCharsets.UTF_8));
             byte[] s = privateSignature.sign();
             return Base64.getEncoder().encodeToString(s);
        }catch(Exception e){
        	LOGGER.error("An error occurrend while trying to encrypt...",e);
        	throw new EncryptionException("An error occurrend while trying to encrypt...",e);
        }
	}
	
	/**
	 * Gets the Private Key stored in project's resources.
	 *
	 * @return a PrivateKey instance that represent the private key
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws GeneralSecurityException the general security exception
	 */
	private PrivateKey getPrivateKey() throws IOException, GeneralSecurityException{
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		File file = new File(classloader.getResource("client-rsa-private-key.cer").getFile());
		String pk = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
		String realPK = pk.replaceAll("-----END PRIVATE KEY-----", StringUtils.EMPTY)
                .replaceAll("-----BEGIN PRIVATE KEY-----", StringUtils.EMPTY)
                .replaceAll("\n", "");
		byte[] b1 = Base64.getDecoder().decode(realPK);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
	}
}
