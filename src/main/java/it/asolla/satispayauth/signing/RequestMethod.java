/**
 * 
 */
package it.asolla.satispayauth.signing;

/**
 * @author andrea.solla
 * @date 13 dic 2018
 */
public enum RequestMethod {
	GET, POST, PUT, DELETE;
}
