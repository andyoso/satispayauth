/**
 * 
 */
package it.asolla.satispayauth.signing;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.asolla.satispayauth.auth.AuthClient;

/**
 * This class offers a provides an utility to generate the Signature String
 *
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class SignatureStringBuilder {
	
	private static final Logger LOGGER = LogManager.getLogger(AuthClient.class);

	public static final String REQUEST_TARGET = "(request-target)";
	
	public static final String HOST = "Host";
	
	public static final String CR = "\n";
	
	public static final String CR_WIN = "\r";
	
	public static final String DATE = "Date";
	
	private List<Header> headers;
	
	private List<String> headersToSign;
	
	private RequestMethod method;
	
	private String path;
	
	/**
	 * Set the Headers of the request.
	 *
	 * @param headers the headers
	 * @return the builder
	 */
	public SignatureStringBuilder headers(Header[] headers){
		this.headers = Arrays.asList(headers);
		return this;
	}
	
	/**
	 * Set the name of the headers to use for the signature
	 *
	 * @param headers the headers
	 * @return the builder
	 */
	public SignatureStringBuilder headerToSign(List<String> headers){
		this.headersToSign = headers;
		return this;
	}
	
	/**
	 * Set the HTTP Method.
	 *
	 * @param method the method
	 * @return the builder
	 */
	public SignatureStringBuilder method(RequestMethod method){
		this.method = method;
		return this;
	}
	
	/**
	 * Set the path of the WS
	 *
	 * @param path the path
	 * @return the builder
	 */
	public SignatureStringBuilder path(String path){
		this.path = path;
		return this;
	}
	
	/**
	 * Build the Signature String
	 *
	 * @return the Signature String
	 */
	public String build(){

		if(headers.stream().noneMatch(header -> StringUtils.equals(header.getName(),DATE))){
			throw new IllegalArgumentException("No "+DATE+" header found!");
		}
		LOGGER.info("Building the Signature String...");

		StringBuilder sb = new StringBuilder();
		StringBuilder sbreqTarg = new StringBuilder();
		sbreqTarg.append(StringUtils.lowerCase(method.name())).append(' ').append(path);

		for(int i = 0; i<headersToSign.size();i++){
			final String headerToSign = headersToSign.get(i);
			if(StringUtils.equals(headerToSign,REQUEST_TARGET)){
				sb.append(getHeaderString(REQUEST_TARGET, sbreqTarg.toString()));
				sb.append(CR);
			}else{
				Header header = headers.stream().filter(h-> StringUtils.equalsIgnoreCase(h.getName(),headerToSign )).findFirst().orElse(null);
				if(header==null){
					throw new IllegalArgumentException("Header "+headerToSign+" not found!");
				}
				String value = StringUtils.replace(StringUtils.replace(
						Objects.toString(header.getValue(),"null"),CR ,StringUtils.EMPTY),	CR_WIN,	StringUtils.EMPTY);
				sb.append(getHeaderString(StringUtils.lowerCase(header.getName()),value));
				if(i!=headersToSign.size()-1){
					sb.append(CR);
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Gets the header string.
	 *
	 * @param name the name
	 * @param value the value
	 * @return the header string
	 */
	private String getHeaderString(String name,String value) {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append(": ");
		sb.append(value);
		return sb.toString();
	}



}
