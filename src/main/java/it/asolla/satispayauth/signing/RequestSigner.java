/**
 * 
 */
package it.asolla.satispayauth.signing;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.asolla.satispayauth.crypto.Cypher;

/**
 * This class implements the HttpRequestInterceptor and adds the authentication 
 * header in the request to Satispay
 * @author andrea.solla
 * @date 13 dic 2018
 */

public class RequestSigner implements HttpRequestInterceptor{
	private static final Logger LOGGER = LogManager.getLogger(RequestSigner.class);

	private List<String> headersToSign;
	private String algorithm;
	private String keyId;
	public RequestSigner(String keyId,String algorithm,List<String> headersToSign) {
		this.headersToSign = headersToSign;
		this.keyId = keyId;
		this.algorithm = algorithm;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.http.HttpRequestInterceptor#process(org.apache.http.HttpRequest, org.apache.http.protocol.HttpContext)
	 */
	@Override
	public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
		SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Cypher cypher = new Cypher();
		request.addHeader(new BasicHeader("Date", sdf.format(new Date())));
		
		String signatureString = new SignatureStringBuilder()
				.headers(request.getAllHeaders())
				.headerToSign(headersToSign)
				.method(getMethod(request))
				.path(getPath(request))
				.build();
		LOGGER.info("Signature String -->\n"+signatureString);
		String auth = buildAuthorizationHeader(cypher.encrypt(signatureString));
		request.addHeader(new BasicHeader("Authorization", auth));
		LOGGER.info("Method --> "+getMethod(request));
		LOGGER.info("Headers --> ");
		for(Header header : request.getAllHeaders()){
			LOGGER.info(header.getName()+" : "+header.getValue());
		}
		
		
	}
	
	private RequestMethod getMethod(HttpRequest request){
		return RequestMethod.valueOf(request.getRequestLine().getMethod());
	}
	
	private String getPath(HttpRequest request){
		return request.getRequestLine().getUri();
	}

	private String buildAuthorizationHeader(String signature){
		StringBuilder sb = new StringBuilder();
		sb.append("Signature ")
		.append("keyId=\"").append(keyId).append("\", ")
		.append("algorithm=\"").append(algorithm).append("\", ")
		.append("headers=\"").append(String.join(" ", headersToSign).toLowerCase()).append("\", ")
		.append("signature=\"").append(signature).append("\", ")
		.append("satispaysequence=\"").append("4").append("\", ")
		.append("satispayperformance=\"").append("LOW").append("\"");
		
		return sb.toString();
	}
	
}
