/**
 * 
 */
package it.asolla.satispayauth.auth.impl;

import java.net.URI;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

import it.asolla.satispayauth.auth.AuthClient;

/**
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class DeleteAuthClient  extends AuthClient {

	/* (non-Javadoc)
	 * @see it.asolla.satispayauth.signing.AuthClient#getHttpRequest(java.net.URI)
	 */
	@Override
	protected HttpRequestBase getHttpRequest(URI uri) {
		return new HttpDelete(uri);
	}

}
