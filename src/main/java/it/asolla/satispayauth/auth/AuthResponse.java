/**
 * 
 */
package it.asolla.satispayauth.auth;

/**
 * This class contains the Satispay server response
 * 
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class AuthResponse {
	private Integer status;
	private String body;

	
	public AuthResponse(Integer status, String body) {
		this.status = status;
		this.body = body;
	}
	
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

}
