/**
 * 
 */
package it.asolla.satispayauth.auth;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.asolla.satispayauth.signing.RequestSigner;

/**
 * This abstract class provides the method to invoke Satispay gateway
 * @author andrea.solla
 * @date 13 dic 2018
 */
public abstract class AuthClient {
	
	private static final Logger LOGGER = LogManager.getLogger(AuthClient.class);
	
	private String url;
	
	private String signAlgorithm;
	
	private String keyId;
	
	private List<String> headersToSign;
	
	private List<Header> headers;
	
	/**
	 * Adds the name of a header to use during the signature.
	 *
	 * @param headername the header name
	 * @return the client
	 */
	public AuthClient addHeaderToSign(String headername){
		if(headersToSign==null){
			headersToSign = new ArrayList<>();
		}
		headersToSign.add(headername);
		return this;
	}
	
	/**
	 * Adds a new header to use during the request
	 *
	 * @param header the header
	 * @return the client
	 */
	public AuthClient addHeader(Header header){
		if(headers==null){
			headers = new ArrayList<>();
		}
		headers.add(header);
		return this;
	}
	
	/**
	 * The Url of the WS.
	 *
	 * @param url the url
	 * @return the client
	 */
	public AuthClient url(String url){
		this.url = url;
		return this;
	}

	/**
	 * Set the algorithm to use to sign
	 * For this exercise it should be always rsa-sha256
	 *
	 * @param signAlgorithm the sign algorithm
	 * @return the client
	 */
	public AuthClient signAlgorithm(String signAlgorithm){
		this.signAlgorithm = signAlgorithm;
		return this;
	}
	
	
	
	/**
	 * Set Key id to use to authenticate
	 *
	 * @param keyId the key id
	 * @return the client
	 */
	public AuthClient keyId(String keyId){
		this.keyId = keyId;
		return this;
	}
	
	/**
	 * Gets the http request.
	 *
	 * @param uri the uri
	 * @return the http request
	 */
	protected abstract HttpRequestBase getHttpRequest(URI uri);
	
	
	
	/**
	 * Execute the HTTP request.
	 *
	 * @return the response of the server
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public AuthResponse execute() throws IOException{
		LOGGER.info("################# Authentication ##################");
		URI uri = URI.create(url); 
		HttpClientBuilder builder = HttpClientBuilder.create();
		builder.addInterceptorLast(new RequestSigner(keyId,signAlgorithm,headersToSign));
		try (CloseableHttpClient client = builder.build()) {
			HttpRequestBase http = getHttpRequest(uri);			
			if(headers!=null){
				for(Header header : headers){
					http.addHeader(header);
				}
			}
			try (CloseableHttpResponse response = client.execute(http)) {
				ResponseHandler<String> handler = new BasicResponseHandler();
				String body = handler.handleResponse(response);
				int code = response.getStatusLine().getStatusCode();
				return new AuthResponse(code, body);
			}
		}
	}
}
