package it.asolla.satispayauth;

import java.io.IOException;

import org.apache.http.message.BasicHeader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import it.asolla.satispayauth.auth.AuthClient;
import it.asolla.satispayauth.auth.AuthResponse;
import it.asolla.satispayauth.auth.impl.DeleteAuthClient;
import it.asolla.satispayauth.auth.impl.GetAuthClient;
import it.asolla.satispayauth.auth.impl.PostAuthClient;
import it.asolla.satispayauth.auth.impl.PutAuthClient;
import it.asolla.satispayauth.signing.RequestMethod;

/**
 * The Main Class that calls the authentication client
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class App  	
{
	
	private static final Logger LOGGER = LogManager.getLogger(App.class);
	private static final String SIGN_ALGO = "rsa-sha256";
	private static final String URL = "https://staging.authservices.satispay.com/wally-services/protocol/tests/signature";
	
	
	public static void main( String[] args ) throws IOException 
	{		
		RequestMethod method = RequestMethod.PUT;
		AuthResponse response = getAuthClient(method)
				.url(URL)
				.signAlgorithm(SIGN_ALGO)
				.keyId("signature-test-66289")
				.addHeader(new BasicHeader("NiceHeader","nice"))
				.addHeaderToSign("(request-target)")
				.addHeaderToSign("date")
				.addHeaderToSign("NiceHeader")
				.execute();
		
		LOGGER.info("Authentication results : Status:"+response.getStatus()+" Body:"+response.getBody());	
	}
	
	
	private static AuthClient getAuthClient(RequestMethod method){
		switch (method) {
			case GET: return new GetAuthClient();
			case POST: return new PostAuthClient();
			case PUT: return new PutAuthClient();
			case DELETE: return new DeleteAuthClient();
		default:
			throw new IllegalArgumentException("Unknown HTTP Method!");
		}
	}
}
