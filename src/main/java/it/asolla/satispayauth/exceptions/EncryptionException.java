/**
 * 
 */
package it.asolla.satispayauth.exceptions;

/**
 * @author andrea.solla
 * @date 13 dic 2018
 */
public class EncryptionException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -676467246095076540L;

	public EncryptionException(String msg,Throwable causedBy){
		super(msg,causedBy);
	}
}
